Currencies 
------------

Installation
------------

#### Requirements

* php: 7.1.x
* Composer globally 

 
```bash
$ git clone https://AgnieszkaZadlo93@bitbucket.org/AgnieszkaZadlo93/currencies.git
$ cd project
$ cp .env.dist .env
$ composer install
$ php bin/console server:start

``` 


#### Database schema : 
* check REQUIREMENTS.md

#### Usage: 
route: `/api/currency/calculate`

request data
``` 
{
	"base_currency": "PLN",
	"dest_currency": "EUR",
	"exchange_rate": 124
}
```

sucessful response data: 
```
{
    "code": 200,
    "data": 531.8
}
```
failure response data: 
```
{
   "code": 422,
   "message": "Invalid data in request body",
   "errors": {
       "exchange_rate": [
       "This value should be greater than or equal to 0."
        ]
    }
}
```
   
   
```
{
    "code": 400,
    "message": "Exchange rate missing in database"
}
```