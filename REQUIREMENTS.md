Zadanie polega na napisaniu prostego kalkulatora walutowego przy użyciu Laravela, Symfony lub czystego PHP. Wystarczy nam jeden endpoint, w którym użytkownik na wejściu podaje następujące parametry:


`base_currency` - kod (np. GBP) waluty, z której ma nastąpić przeliczenie
`dest_currency` - kod (np. PLN) waluty, na którą ma nastąpić przeliczenie
`exchange_rate` - kwota do przewalutowania

W odpowiedzi powinniśmy dostać przeliczoną wartość (wedle najświeższego kursu dostępnego w bazie danych) w formacie zgodnym z JSONAPI. W załączeniu przesyłam strukturę bazy danch do tego zadania.

Struktura bazy danych: 
```

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `currencies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `currencies` ;

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(3) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `exchange_rates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `exchange_rates` ;

CREATE TABLE IF NOT EXISTS `exchange_rates` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `base_currency_id` INT UNSIGNED NOT NULL,
  `dest_currency_id` INT UNSIGNED NOT NULL,
  `exchange_rate` DECIMAL(10,4) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_exchange_rates_currencies1_idx` (`base_currency_id` ASC),
  INDEX `fk_exchange_rates_currencies2_idx` (`dest_currency_id` ASC),
  CONSTRAINT `fk_exchange_rates_currencies1`
    FOREIGN KEY (`base_currency_id`)
    REFERENCES `currencies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_exchange_rates_currencies2`
    FOREIGN KEY (`dest_currency_id`)
    REFERENCES `currencies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- Data for table `currencies`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `currencies` (`id`, `code`) VALUES (1, 'PLN');
INSERT INTO `currencies` (`id`, `code`) VALUES (2, 'EUR');
INSERT INTO `currencies` (`id`, `code`) VALUES (3, 'USD');
INSERT INTO `currencies` (`id`, `code`) VALUES (4, 'GBP');

COMMIT;


-- -----------------------------------------------------
-- Data for table `exchange_rates`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `exchange_rates` (`id`, `date`, `base_currency_id`, `dest_currency_id`, `exchange_rate`) VALUES (1, '2018-06-15', 1, 2, 4.2802);
INSERT INTO `exchange_rates` (`id`, `date`, `base_currency_id`, `dest_currency_id`, `exchange_rate`) VALUES (2, '2018-06-15', 1, 3, 3.6922);
INSERT INTO `exchange_rates` (`id`, `date`, `base_currency_id`, `dest_currency_id`, `exchange_rate`) VALUES (3, '2018-06-15', 1, 4, 4.8998);
INSERT INTO `exchange_rates` (`id`, `date`, `base_currency_id`, `dest_currency_id`, `exchange_rate`) VALUES (4, '2018-06-18', 1, 2, 4.2887);
INSERT INTO `exchange_rates` (`id`, `date`, `base_currency_id`, `dest_currency_id`, `exchange_rate`) VALUES (5, '2018-06-18', 1, 3, 3.7003);
INSERT INTO `exchange_rates` (`id`, `date`, `base_currency_id`, `dest_currency_id`, `exchange_rate`) VALUES (6, '2018-06-18', 1, 4, 4.9003);

COMMIT;
```