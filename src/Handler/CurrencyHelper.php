<?php

namespace App\Handler;

use App\Entity\ExchangeRate;
use App\Exception\ExchangeRateNotFoundException;
use App\Repository\CurrencyRepository;
use App\Repository\ExchangeRateRepository;

/**
 * Class CurrencyHelper
 * @package App\Handler
 */
class CurrencyHelper
{
    /** @var CurrencyRepository */
    private $currencyRepository;

    /**  @var ExchangeRateRepository */
    private $exchangeRateRepository;

    /**
     * CurrencyHelper constructor.
     * @param CurrencyRepository $currencyRepository
     * @param ExchangeRateRepository $exchangeRateRepository
     */
    public function __construct(
        CurrencyRepository $currencyRepository,
        ExchangeRateRepository $exchangeRateRepository
    )
    {
        $this->currencyRepository = $currencyRepository;
        $this->exchangeRateRepository = $exchangeRateRepository;
    }

    /**
     * @param array $requestParameters
     * @return float
     */
    public function handle(array $requestParameters): float
    {
        $baseCurrency = $this->currencyRepository->findOneBy(['code' => $requestParameters['base_currency']]);
        $destCurrency = $this->currencyRepository->findOneBy(['code' => $requestParameters['dest_currency']]);

        /** @var ExchangeRate $exchangeRate */
        $exchangeRate = $this->exchangeRateRepository->findOneBy(['baseCurrency' => $baseCurrency->getId(), 'destCurrency' => $destCurrency->getId()], ['date' => 'DESC']);

        if (!$exchangeRate) {
            throw new ExchangeRateNotFoundException();
        }
        return $this->calculateValue($requestParameters['exchange_rate'], $exchangeRate->getExchangeRate());

    }

    /**
     * @param float $amount
     * @param float $exchangeRate
     * @return float
     */
    public function calculateValue(float $amount, float $exchangeRate): float
    {
        return round($amount * $exchangeRate, 2);
    }
}