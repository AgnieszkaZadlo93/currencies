<?php

namespace App\Controller;

use App\Form\Type\CalculateType;
use App\Handler\CurrencyHelper;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\RouteResource("currency")
 * Class CurrencyController
 * @package App\Controller
 */
final class CurrencyController extends FOSRestController
{

    /**  @var CurrencyHelper */
    private $currencyHelper;

    /**
     * CurrencyController constructor.
     * @param CurrencyHelper $currencyHelper
     */
    public function __construct(CurrencyHelper $currencyHelper)
    {
        $this->currencyHelper = $currencyHelper;
    }

    /**
     * @Rest\Post("/currency/calculate")
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     * @throws \App\Exception\ExchangeRateNotFoundException
     */
    public function calculateAction(Request $request)
    {
        $form = $this->createForm(CalculateType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            $errors = $this->getErrorsFromForm($form);

            $templateData = [
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => 'Invalid data in request body',
                'errors' => $errors
            ];
            return $this->view($templateData, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $data = $this->currencyHelper->handle($request->request->all());

        $templateData = [
            'code' => Response::HTTP_OK,
            'data' => $data,
        ];

        return $this->view($templateData, Response::HTTP_OK);
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    private function getErrorsFromForm(FormInterface $form): array
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }
}