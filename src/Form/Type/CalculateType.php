<?php


namespace App\Form\Type;

use App\Repository\CurrencyRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CalculateForm
 * @package App\Form
 */
final class CalculateType extends AbstractType
{
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * CalculateForm constructor.
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $availableCurrencies = array_column($this->currencyRepository->findAvailableCurrencies(), 'code');
        $builder
            ->add('base_currency', ChoiceType::class, [
                'choices' => $availableCurrencies,
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => ['default'],
                    ])
                ]
            ])
            ->add('dest_currency', ChoiceType::class, [
                'choices' => $availableCurrencies,
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => ['default'],
                    ]),

                ]
            ])
            ->add('exchange_rate', NumberType::class, [
                'constraints' => [
                    new Assert\NotBlank([
                        'groups' => ['default'],
                    ]),
                    new Assert\GreaterThanOrEqual([
                        'value' => 0,
                        'groups' => ['default'],
                    ])
                ]
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            "allow_extra_fields" => false,
            "validation_groups" => 'default'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'calculate_currency';
    }

}