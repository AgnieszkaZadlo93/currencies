<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExchangeRates
 *
 * @ORM\Table(name="exchange_rates", indexes={@ORM\Index(name="fk_exchange_rates_Currency1_idx", columns={"base_currency_id"}), @ORM\Index(name="fk_exchange_rates_Currency2_idx", columns={"dest_currency_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\ExchangeRateRepository")
 */
class ExchangeRate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="exchange_rate", type="decimal", precision=10, scale=4, nullable=false)
     */
    private $exchangeRate;


    /**
     * @var \Currency
     *
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="base_currency_id", referencedColumnName="id")
     * })
     */
    private $baseCurrency;

    /**
     * @var \Currency
     *
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dest_currency_id", referencedColumnName="id")
     * })
     */
    private $destCurrency;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     * @return ExchangeRate
     */
    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Currency|null
     */
    public function getBaseCurrency(): ?Currency
    {
        return $this->baseCurrency;
    }

    /**
     * @param Currency|null $baseCurrency
     */
    public function setBaseCurrencyId(?Currency $baseCurrency): void
    {
        $this->baseCurrency = $baseCurrency;

    }

    /**
     * @return Currency|null
     */
    public function getDestCurrency(): ?Currency
    {
        return $this->destCurrency;
    }

    /**
     * @param Currency|null $destCurrency
     */
    public function setDestCurrency(?Currency $destCurrency): void
    {
        $this->destCurrency = $destCurrency;

    }

    /**
     * @return string
     */
    public function getExchangeRate(): string
    {
        return $this->exchangeRate;
    }

    /**
     * @param string $exchangeRate
     */
    public function setExchangeRate(string $exchangeRate): void
    {
        $this->exchangeRate = $exchangeRate;
    }
}
