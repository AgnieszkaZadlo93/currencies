<?php

namespace App\Exception;

/**
 * Class ExchangeRateNotFound
 * @package App\Exception
 */
class ExchangeRateNotFoundException extends \Exception
{
    public function __construct($message = "Exchange rate missing in database")
    {
        parent::__construct($message, 400);
    }
}
